package com.starbucks.astapley.sbux.profile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.UiThreadTestRule
import com.starbucks.astapley.sbux.MainActivity
import com.starbucks.astapley.sbux.R
import com.starbucks.astapley.sbux.helpers.FirebaseAuthRx
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext.loadKoinModules
import org.koin.test.AutoCloseKoinTest
import org.mockito.Mockito.mock

/*
    WIP
 */

@RunWith(AndroidJUnit4::class)
class ProfileFragmentTest : AutoCloseKoinTest() {

    @get:Rule
    val ldRule = InstantTaskExecutorRule()

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @get:Rule
    val uiRule = UiThreadTestRule()

    private val mockAuthRx = mock(FirebaseAuthRx::class.java)
    private val profileViewModel = ProfileViewModel(mockAuthRx)

    private val testModule = module(override = true) {
        single { mockAuthRx }
        viewModel { profileViewModel }
    }

    @Before
    fun setUp() {
        loadKoinModules(listOf(testModule))
        activityRule
                .activity
                .supportFragmentManager
                .beginTransaction()
                .replace(R.id.navController, ProfileFragment(), "ProfileFragment")
                .commit()
    }

    @Test
    fun shouldShowUnauthenticated() {
        profileViewModel.signOut()
        onView(withId(R.id.emailEditText)).check(matches(isDisplayed()))
        onView(withId(R.id.passwordEditText)).check(matches(isDisplayed()))
    }
}
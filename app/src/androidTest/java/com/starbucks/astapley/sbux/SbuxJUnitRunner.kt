package com.starbucks.astapley.sbux

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner

// https://github.com/InsertKoinIO/koin/issues/197#issuecomment-429768448

class SbuxJUnitRunner : AndroidJUnitRunner() {

    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        return super.newApplication(cl, TestApplication::class.java.name, context)
    }
}
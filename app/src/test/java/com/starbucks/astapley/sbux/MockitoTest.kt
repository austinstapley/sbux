package com.starbucks.astapley.sbux

import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class MockitoTest {

    @Test
    fun carTest() {
        val car = mock(Car::class.java)
        `when`(car.drive()).thenReturn(Outcome.OK)

        val actualOutcome = car.drive()

        assertEquals(Outcome.OK, actualOutcome)
    }
}
package com.starbucks.astapley.sbux

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Test

open class Car {
    open fun drive(): Outcome = Outcome.FAIL
}

enum class Outcome {
    OK, FAIL
}

class MockkTest {

    @Test
    fun carTest() {
        val car = mockk<Car>()
        every { car.drive() } returns Outcome.OK

        val actualOutcome = car.drive()

        verify { car.drive() }
        assertEquals(Outcome.OK, actualOutcome)
    }
}
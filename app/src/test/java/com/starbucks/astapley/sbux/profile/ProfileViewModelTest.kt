package com.starbucks.astapley.sbux.profile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.starbucks.astapley.sbux.RxImmediateSchedulerRule
import com.starbucks.astapley.sbux.helpers.FirebaseAuthRx
import com.starbucks.astapley.sbux.profile.ProfileValidationError.EMAIL_EMPTY
import com.starbucks.astapley.sbux.profile.ProfileValidationError.PASSWORD_EMPTY
import com.starbucks.astapley.sbux.profile.ProfileViewState.Authenticated
import com.starbucks.astapley.sbux.profile.ProfileViewState.AuthenticationFailed
import com.starbucks.astapley.sbux.profile.ProfileViewState.ProfileLoading
import com.starbucks.astapley.sbux.profile.ProfileViewState.Unauthenticated
import com.starbucks.astapley.sbux.profile.ProfileViewState.ValidationFailed
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.`when` as whenever

/*
    100% test coverage - don't believe me? Run with Coverage and see for yourself.

    Strategy for each test
    - create expected state
    - trigger action passing AuthEvent
    - verify view model in expected state
 */

class ProfileViewModelTest {

    @get:Rule
    val ldRule = InstantTaskExecutorRule()

    @get:Rule
    val rxRule = RxImmediateSchedulerRule()

    private lateinit var profileViewModel: ProfileViewModel
    private val authRx = mock<FirebaseAuthRx>()

    @Before
    fun setUp() {
        whenever(authRx.currentUser).thenReturn(null)
        profileViewModel = ProfileViewModel(authRx)
    }

    @Test
    fun shouldInitializeUnauthenticatedWhenCurrentUserNull() {
        assertState(Unauthenticated)
    }

    @Test
    fun shouldInitializeAuthenticatedWhenCurrentUserExists() {
        val mockUser = mock<FirebaseUser>()
        whenever(authRx.currentUser).thenReturn(mockUser)

        profileViewModel = ProfileViewModel(authRx)

        assertState(Authenticated(mockUser))
    }

    @Test
    fun shouldCatchEmptyEmailOnSignIn() {
        val expectedState = createInvalidInput(EMAIL_EMPTY)
        profileViewModel.signIn(createAuthEvent(email = ""))
        assertState(expectedState)
    }

    @Test
    fun shouldCatchEmptyPasswordOnSignIn() {
        val expectedState = createInvalidInput(PASSWORD_EMPTY)
        profileViewModel.signIn(createAuthEvent(password = ""))
        assertState(expectedState)
    }

    @Test
    fun shouldCatchMultipleErrorsOnSignIn() {
        val expectedState = createInvalidInput(EMAIL_EMPTY, PASSWORD_EMPTY)
        profileViewModel.signIn(createAuthEvent("", ""))
        assertState(expectedState)
    }

    @Test
    fun shouldCatchEmptyEmailOnJoin() {
        val expectedState = createInvalidInput(EMAIL_EMPTY)
        profileViewModel.join(createAuthEvent(email = ""))
        assertState(expectedState)
    }

    @Test
    fun shouldCatchEmptyPasswordOnJoin() {
        val expectedState = createInvalidInput(PASSWORD_EMPTY)
        profileViewModel.join(createAuthEvent(password = ""))
        assertState(expectedState)
    }

    @Test
    fun shouldCatchMultipleErrorsOnJoin() {
        val expectedState = createInvalidInput(EMAIL_EMPTY, PASSWORD_EMPTY)
        profileViewModel.join(createAuthEvent("", ""))
        assertState(expectedState)
    }

    @Test
    fun shouldShowLoadingOnSignIn() {
        /*
            Using Observable.empty() here made it so no item was emitted
            but we still started loading as though we fired off a request
         */
        whenever(authRx.signIn(any())).thenReturn(Observable.empty())
        profileViewModel.signIn(createAuthEvent())
        assertState(ProfileLoading)
    }

    @Test
    fun shouldShowLoadingOnJoin() {
        whenever(authRx.join(any())).thenReturn(Observable.empty())
        profileViewModel.join(createAuthEvent())
        assertState(ProfileLoading)
    }

    @Test
    fun shouldAuthenticateOnSignIn() {
        val mockUser = createMockUser()
        stubAuthResult(mockUser)

        profileViewModel.signIn(createAuthEvent())

        assertState(Authenticated(mockUser))
    }

    @Test
    fun shouldCreateAccountOnJoin() {
        val mockUser = createMockUser()
        stubAuthResult(mockUser)

        profileViewModel.join(createAuthEvent())

        assertState(Authenticated(mockUser))
    }

    @Test
    fun shouldHandleSignInError() {
        val errorMessage = "Oops, something broke!"
        /*
            A nice way to force an error and later be able to verify its message
         */
        whenever(authRx.signIn(any())).thenReturn(Observable.error(Throwable(errorMessage)))

        profileViewModel.signIn(createAuthEvent())

        assertState(AuthenticationFailed(errorMessage))
    }

    @Test
    fun shouldHandleJoinError() {
        val errorMessage = "Oops, something broke!"
        whenever(authRx.join(any())).thenReturn(Observable.error(Throwable(errorMessage)))

        profileViewModel.join(createAuthEvent())

        assertState(AuthenticationFailed(errorMessage))
    }

    @Test
    fun shouldHandleSignInNullErrorMessage() {
        whenever(authRx.signIn(any())).thenReturn(Observable.error(Throwable()))
        profileViewModel.signIn(createAuthEvent())
        assertState(AuthenticationFailed("Authentication failed for an unknown reason"))
    }

    @Test
    fun shouldHandleJoinNullErrorMessage() {
        whenever(authRx.join(any())).thenReturn(Observable.error(Throwable()))
        profileViewModel.join(createAuthEvent())
        assertState(AuthenticationFailed("Authentication failed for an unknown reason"))
    }

    @Test
    fun shouldHandleSignOut() {
        /*
            This was a new one for me. I realized a previous test I wrote
            already did all the work for getting into an authenticated state
            so just referenced that method and worked from there - I'm not sure
            how common this is but it was a nice find.
         */
        shouldAuthenticateOnSignIn()

        profileViewModel.signOut()

        verify(authRx).signOut()
        assertState(Unauthenticated)
    }

    private fun assertState(expectedState: ProfileViewState) {
        assertEquals(expectedState, profileViewModel.viewState().value)
    }

    private fun createInvalidInput(vararg errors: ProfileValidationError) = ValidationFailed(errors.toList())

    private fun createAuthEvent(email: String = "email", password: String = "password") = AuthEvent(email, password)

    private fun createMockUser(): FirebaseUser {
        val mockUser = mock<FirebaseUser>()
        whenever(mockUser.email).thenReturn("email")
        return mockUser
    }

    private fun stubAuthResult(user: FirebaseUser = createMockUser()) {
        val mockAuthResult = mock<AuthResult>()
        whenever(mockAuthResult.user).thenReturn(user)
        whenever(authRx.signIn(any())).thenReturn(Observable.just(mockAuthResult))
        whenever(authRx.join(any())).thenReturn(Observable.just(mockAuthResult))
    }
}
package com.starbucks.astapley.sbux.profile

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import coil.api.load
import coil.transform.BlurTransformation
import coil.transform.CircleCropTransformation
import coil.transform.GrayscaleTransformation
import coil.transform.RoundedCornersTransformation
import com.google.firebase.auth.FirebaseUser
import com.jakewharton.rxbinding3.view.clicks
import com.starbucks.astapley.sbux.BaseFragment
import com.starbucks.astapley.sbux.R
import com.starbucks.astapley.sbux.invisible
import com.starbucks.astapley.sbux.profile.ProfileSingleEvent.Notification
import com.starbucks.astapley.sbux.profile.ProfileValidationError.EMAIL_EMPTY
import com.starbucks.astapley.sbux.profile.ProfileValidationError.PASSWORD_EMPTY
import com.starbucks.astapley.sbux.profile.ProfileViewState.Authenticated
import com.starbucks.astapley.sbux.profile.ProfileViewState.AuthenticationFailed
import com.starbucks.astapley.sbux.profile.ProfileViewState.ProfileLoading
import com.starbucks.astapley.sbux.profile.ProfileViewState.Unauthenticated
import com.starbucks.astapley.sbux.profile.ProfileViewState.ValidationFailed
import com.starbucks.astapley.sbux.show
import com.starbucks.astapley.sbux.visible
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.profile_authenticated.*
import kotlinx.android.synthetic.main.profile_unauthenticated.*
import org.koin.android.viewmodel.ext.android.viewModel

/*
    Used to capture relevant authentication data.
    It is important to capture authentication data in the view so the
    view model doesn't need a reference to the view.
 */
data class AuthEvent(val email: String, val password: String)

class ProfileFragment : BaseFragment() {

    // using Koin to inject the view model - isn't necessary - just trying out Koin
    private val viewModel by viewModel<ProfileViewModel>()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.viewState().observe(this, Observer { updateViewState(it) })
        Log.v("Austin", "View created - observing $viewModel") // after config change you can see we observe the same view model
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    /*
            I need to build out the ui automation further but having states simplifies
            the espresso tests we need to write. We just need to test that each state performs
            the correct view transformations. This is quite a different approach than what SDETs
            are currently taking.
         */
    private fun updateViewState(state: ProfileViewState) = when (state) {
        ProfileLoading -> loadingView.visible()
        Unauthenticated -> showUnauthenticated()
        is Authenticated -> showAuthenticated(state.user)
        is AuthenticationFailed -> onAuthenticationFailed()
        is ValidationFailed -> showValidationErrors(state.errors)
    }

    private fun handleSingleEvents(event: ProfileSingleEvent) = when (event) {
        is Notification -> container.show(event.message)
    }

    private fun showUnauthenticated() {
        Log.v("Austin", "User is unauthenticated")
        loadingView.invisible()
        profile_unauthenticated.visible()
        profile_authenticated.invisible()
        disposables += signInButton.clicks().subscribe {
            viewModel.signIn(createAuthEvent()).observe(this, Observer { handleSingleEvents(it) })
        }
        disposables += joinButton.clicks().subscribe { viewModel.join(createAuthEvent()) }
    }

    private fun showAuthenticated(user: FirebaseUser) {
        Log.v("Austin", "User is authenticated")
        loadingView.invisible()
        profile_unauthenticated.invisible()
        profile_authenticated.visible()
        emailTextView.text = user.email
        disposables += signOutButton.clicks().subscribe { viewModel.signOut() }

        image1.load("https://miro.medium.com/max/1050/1*cxZhQbf-KuKU_wM8RGgofg.jpeg") {
            crossfade(true)
            transformations(BlurTransformation(this@ProfileFragment.context!!))
        }
        image2.load("https://miro.medium.com/max/1050/1*cxZhQbf-KuKU_wM8RGgofg.jpeg") {
            crossfade(true)
            transformations(CircleCropTransformation())
        }
        image3.load("https://miro.medium.com/max/1050/1*cxZhQbf-KuKU_wM8RGgofg.jpeg") {
            crossfade(true)
            transformations(GrayscaleTransformation())
        }
        image4.load("https://miro.medium.com/max/1050/1*cxZhQbf-KuKU_wM8RGgofg.jpeg") {
            crossfade(true)
            transformations(RoundedCornersTransformation(50f, 50f, 50f, 50f))
        }
    }

    private fun onAuthenticationFailed() {
        Log.v("Austin", "Authentication error occurred")
        loadingView.invisible()
        showUnauthenticated()
    }

    private fun showValidationErrors(errors: List<ProfileValidationError>) {
        Log.v("Austin", "Invalid input entered")
        errors.forEach { error ->
            when (error) {
                EMAIL_EMPTY -> emailEditText.error = EMAIL_EMPTY.message
                PASSWORD_EMPTY -> passwordEditText.error = PASSWORD_EMPTY.message
            }
        }
    }

    private fun createAuthEvent() = AuthEvent(emailEditText.text.toString(), passwordEditText.text.toString())
}

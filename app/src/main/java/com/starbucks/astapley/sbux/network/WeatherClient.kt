package com.starbucks.astapley.sbux.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.starbucks.astapley.sbux.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

object WeatherClient {

    private val client = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
            })
            .build()

    private val retrofitBuilder = Retrofit.Builder()
            .baseUrl("https://api.darksky.net/")
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())

    val weatherServiceRx: WeatherServiceRx = retrofitBuilder
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(WeatherServiceRx::class.java)

    val weatherService: WeatherService = retrofitBuilder
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(WeatherService::class.java)
}
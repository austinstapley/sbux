package com.starbucks.astapley.sbux

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RxSchedulers {
    val main: Scheduler = AndroidSchedulers.mainThread()
    val io: Scheduler = Schedulers.io()
}
package com.starbucks.astapley.sbux

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : Fragment() {

    /*
        I tried to not push too much into base classes as it increases
        flexibility in the architecture rather than forcing an approach.
        This flexibility could become a problem when deviating from
        the standard pattern - something we'd need to weigh.
        The limited things in this class are just for convenience.
     */

    protected val disposables = CompositeDisposable()

    fun navigate(@IdRes fragmentId: Int) {
        view?.findNavController()?.navigate(fragmentId)
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }
}
package com.starbucks.astapley.sbux

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {

    /*
        Again, I didn't want to push too much into this class.
        I considered using generics to pass a view state type
        so an instance of live data could be automatically created
        for you but ultimately scraped this as it's not much additional
        overhead to leave in the view model itself and we could eventually
        run into cases where we might want to expose multiple live data
     */

    protected val disposables = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        Log.v("Austin", "onCleared - clearing disposables")
        disposables.dispose()
    }
}
package com.starbucks.astapley.sbux.network

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Forecast(val latitude: Float,
                    val longitude: Float,
                    val timezone: String,
                    val currently: Currently)
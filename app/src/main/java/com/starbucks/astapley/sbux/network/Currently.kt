package com.starbucks.astapley.sbux.network

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Currently(val summary: String,
                     val temperature: Float,
                     val apparentTemperature: Float,
                     val humidity: Float,
                     val windSpeed: Float,
                     val visibility: Float)
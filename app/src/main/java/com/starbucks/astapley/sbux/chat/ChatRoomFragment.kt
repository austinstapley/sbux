package com.starbucks.astapley.sbux.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding3.view.clicks
import com.starbucks.astapley.sbux.BaseFragment
import com.starbucks.astapley.sbux.R
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_chat_room.*

class ChatRoomFragment : BaseFragment() {

    private val incrementor = PublishSubject.create<Unit>()
    private var counter = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_room, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        chatRecyclerView.layoutManager = LinearLayoutManager(context)
        chatRecyclerView.adapter = ChatAdapter(incrementor)
        disposables += incrementor.subscribe {
            counterTextView.text = (++counter).toString()
        }
    }
}

class ChatViewHolder(v: View) : RecyclerView.ViewHolder(v) {

    val incrementButton = v.findViewById<Button>(R.id.incrementButton)
    lateinit var disposable: Disposable

    fun bind(incrementor: PublishSubject<Unit>) {
        disposable = incrementButton.clicks()
                .subscribe { incrementor.onNext(Unit) }
    }

    fun unBind() {
        disposable.dispose()
    }
}

class ChatAdapter(private val incrementor: PublishSubject<Unit>) : RecyclerView.Adapter<ChatViewHolder>() {

    override fun getItemCount() = 100

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.chat_cell, parent, false)
        return ChatViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.bind(incrementor)
//        Log.v("Austin", "Bound " + holder.toString())
    }

    override fun onViewRecycled(holder: ChatViewHolder) {
        super.onViewRecycled(holder)
//        Log.v("Austin", "Unbound " + holder.toString())
        holder.unBind()
    }
}

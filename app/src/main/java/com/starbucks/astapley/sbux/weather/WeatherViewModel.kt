package com.starbucks.astapley.sbux.weather

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.starbucks.astapley.sbux.dependency
import com.starbucks.astapley.sbux.network.Forecast
import com.starbucks.astapley.sbux.network.WeatherService
import com.starbucks.astapley.sbux.weather.WeatherViewState.WeatherError
import com.starbucks.astapley.sbux.weather.WeatherViewState.WeatherLoading
import com.starbucks.astapley.sbux.weather.WeatherViewState.WeatherResult
import kotlinx.coroutines.Dispatchers.IO

sealed class WeatherViewState {
    object WeatherLoading : WeatherViewState()
    data class WeatherResult(val forecast: Forecast) : WeatherViewState()
    data class WeatherError(val message: String) : WeatherViewState()
}

class WeatherViewModel : ViewModel() {

    fun getForecast() = liveData(viewModelScope.coroutineContext + IO) {
        emit(WeatherLoading)
        try {
            emit(WeatherResult(dependency<WeatherService>().getForecast()))
        } catch (e: Exception) {
            emit(WeatherError("Oops, something went wrong!"))
        }
    }
}
package com.starbucks.astapley.sbux.helpers

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.starbucks.astapley.sbux.profile.AuthEvent
import io.reactivex.Observable

fun Task<AuthResult>.toObservable() = Observable.create<AuthResult> { emitter ->
    this.addOnSuccessListener { emitter.onNext(it) }
            .addOnFailureListener { emitter.onError(it) }
            .addOnCompleteListener { emitter.onComplete() }
}

open class FirebaseAuthRx {

    private val auth: FirebaseAuth
        get() = FirebaseAuth.getInstance()

    val currentUser: FirebaseUser?
        get() = auth.currentUser

    fun join(event: AuthEvent): Observable<AuthResult> =
            auth.createUserWithEmailAndPassword(event.email, event.password).toObservable()

    fun signIn(event: AuthEvent): Observable<AuthResult> =
            auth.signInWithEmailAndPassword(event.email, event.password).toObservable()

    fun signOut() {
        auth.signOut()
    }
}
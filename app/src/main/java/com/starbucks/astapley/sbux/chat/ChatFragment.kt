package com.starbucks.astapley.sbux.chat

import android.graphics.drawable.AnimatedVectorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.starbucks.astapley.sbux.BaseFragment
import com.starbucks.astapley.sbux.R
import kotlinx.android.synthetic.main.fragment_chat.*

class ChatFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        chatRoomButton.setOnClickListener { navigate(R.id.chatRoomFragment) }
        (jetpackDroid.drawable as AnimatedVectorDrawable).start()
        (android.drawable as AnimatedVectorDrawable).start()
    }
}

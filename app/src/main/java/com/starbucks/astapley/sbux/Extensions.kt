package com.starbucks.astapley.sbux

import android.view.View
import android.view.animation.AnticipateInterpolator
import com.google.android.material.snackbar.Snackbar
import org.koin.standalone.StandAloneContext

fun View.show(message: String, duration: Int = Snackbar.LENGTH_LONG, actionText: String? = null, action: ((View) -> Unit)? = null) {
    val snackbar = Snackbar.make(this, message, duration)
    actionText?.let { snackbar.setAction(actionText, action) }
    snackbar.show()
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}
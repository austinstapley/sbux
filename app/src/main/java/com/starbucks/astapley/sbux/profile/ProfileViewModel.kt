package com.starbucks.astapley.sbux.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import com.starbucks.astapley.sbux.BaseViewModel
import com.starbucks.astapley.sbux.getKoin
import com.starbucks.astapley.sbux.helpers.FirebaseAuthCoroutine
import com.starbucks.astapley.sbux.helpers.FirebaseAuthResult
import com.starbucks.astapley.sbux.helpers.FirebaseAuthRx
import com.starbucks.astapley.sbux.profile.ProfileSingleEvent.Notification
import com.starbucks.astapley.sbux.profile.ProfileValidationError.EMAIL_EMPTY
import com.starbucks.astapley.sbux.profile.ProfileValidationError.PASSWORD_EMPTY
import com.starbucks.astapley.sbux.profile.ProfileViewState.Authenticated
import com.starbucks.astapley.sbux.profile.ProfileViewState.AuthenticationFailed
import com.starbucks.astapley.sbux.profile.ProfileViewState.ProfileLoading
import com.starbucks.astapley.sbux.profile.ProfileViewState.Unauthenticated
import com.starbucks.astapley.sbux.profile.ProfileViewState.ValidationFailed
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.observers.DisposableObserver
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers.io
import kotlinx.coroutines.Dispatchers.IO

// Various states the profile view needs to handle
sealed class ProfileViewState {
    object ProfileLoading : ProfileViewState()
    object Unauthenticated : ProfileViewState()
    data class Authenticated(val user: FirebaseUser) : ProfileViewState()
    object AuthenticationFailed : ProfileViewState()
    data class ValidationFailed(val errors: List<ProfileValidationError>) : ProfileViewState()
}

sealed class ProfileSingleEvent {
    class Notification(val message: String) : ProfileSingleEvent()
}

// Various input validation errors being caught
enum class ProfileValidationError(val message: String) {
    EMAIL_EMPTY("Please enter an email"),
    PASSWORD_EMPTY("Please enter a password")
}

/*
    I could've taken a different approach here. I decided to go with constructor injection
    vs property injection. Constructor injection (when there aren't a ton of dependencies)
    makes it simple to supply mocks to unit tests. For a property injection example using
    Koin see https://insert-koin.io/docs/1.0/getting-started/junit-test/
    This would be something to explore at some point but in the majority of cases constructor
    injection works fine and I'd say is the recommended practice we should follow.
 */
class ProfileViewModel(private val authRx: FirebaseAuthRx) : BaseViewModel() {

    private val viewState = MutableLiveData<ProfileViewState>()

    /*
        I played around with a few ways of initializing the viewState but this seemed to
        work the best. Since the view model can be reused, initializing in init ensures we
        init once and on view model reuse we don't initialize again
     */
    init {
        viewState.value = authRx.currentUser?.let { Authenticated(it) } ?: Unauthenticated
    }

    // Exposing viewState as LiveData for immutability
    fun viewState(): LiveData<ProfileViewState> = viewState

    fun join(event: AuthEvent) {
        val errors = validateInput(event)
        if (errors.isEmpty()) {
            viewState.value = ProfileLoading
            disposables += authRx.join(event)
                .subscribeOn(io())
                .observeOn(mainThread())
                .subscribeWith(authSubscriber())
        } else {
            viewState.value = ValidationFailed(errors)
        }
    }

    fun signIn(event: AuthEvent) =
        liveData<ProfileSingleEvent>(viewModelScope.coroutineContext + IO) {
            val errors = validateInput(event)
            if (errors.isEmpty()) {
                viewState.postValue(ProfileLoading)
                val result = getKoin().get<FirebaseAuthCoroutine>().signIn(event)
                when (result) {
                    is FirebaseAuthResult.Success -> viewState.postValue(Authenticated(result.user))
                    is FirebaseAuthResult.Failure -> {
                        viewState.postValue(AuthenticationFailed)
                        emit(Notification(result.errorMessage))
                    }
                }
            } else {
                viewState.postValue(ValidationFailed(errors))
            }
        }

    fun signOut() {
        authRx.signOut()
        viewState.value = Unauthenticated
    }

    private fun validateInput(event: AuthEvent): List<ProfileValidationError> {
        val errors = mutableListOf<ProfileValidationError>()
        if (event.email.isEmpty()) errors.add(EMAIL_EMPTY)
        if (event.password.isEmpty()) errors.add(PASSWORD_EMPTY)
        return errors
    }

    private fun authSubscriber() = object : DisposableObserver<AuthResult>() {
        override fun onComplete() {}

        override fun onNext(result: AuthResult) {
            viewState.value = Authenticated(result.user!!)
        }

        override fun onError(error: Throwable) {
            viewState.value =
                AuthenticationFailed //(error.message ?: "Authentication failed for an unknown reason")
        }
    }
}
package com.starbucks.astapley.sbux.network

import retrofit2.http.GET

interface WeatherService {

    @GET("forecast/a563adb0936ea9f9195d0f0af75d3b4e/47.580749,-122.335011?exclude=minutely,hourly,daily,alerts")
    suspend fun getForecast(): Forecast
}
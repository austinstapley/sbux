package com.starbucks.astapley.sbux

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

suspend fun <T> withIoContext(block: suspend CoroutineScope.() -> T): T =
    withContext(getKoin().get<CoroutineDispatchers>().io, block)

suspend fun <T> withDefaultContext(block: suspend CoroutineScope.() -> T): T =
    withContext(getKoin().get<CoroutineDispatchers>().default, block)

class CoroutineDispatchers {
    val main: CoroutineDispatcher = Dispatchers.Main
    val io: CoroutineDispatcher = Dispatchers.IO
    val default: CoroutineDispatcher = Dispatchers.Default
}
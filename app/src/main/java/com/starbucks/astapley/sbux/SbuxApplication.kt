package com.starbucks.astapley.sbux

import android.app.Application
import com.starbucks.astapley.sbux.helpers.FirebaseAuthCoroutine
import com.starbucks.astapley.sbux.helpers.FirebaseAuthRx
import com.starbucks.astapley.sbux.network.WeatherClient
import com.starbucks.astapley.sbux.profile.ProfileViewModel
import com.starbucks.astapley.sbux.weather.WeatherViewModel
import com.starbucks.astapley.sbux.weather.WeatherViewState
import org.koin.android.ext.android.startKoin
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext

val appModule = module {
    single { FirebaseAuthRx() }
    single { FirebaseAuthCoroutine() }
    single { WeatherClient.weatherServiceRx }
    single { WeatherClient.weatherService }
    factory { RxSchedulers() }
    factory { CoroutineDispatchers() }
    viewModel { ProfileViewModel(get()) }
    viewModel { WeatherViewModel() }
}

fun getKoin() = StandAloneContext.getKoin().koinContext

inline fun <reified T : Any> dependency(): T = StandAloneContext.getKoin().koinContext.get()

class SbuxApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule))
    }
}
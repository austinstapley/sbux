package com.starbucks.astapley.sbux.weather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.starbucks.astapley.sbux.BaseFragment
import com.starbucks.astapley.sbux.R
import com.starbucks.astapley.sbux.gone
import com.starbucks.astapley.sbux.network.Forecast
import com.starbucks.astapley.sbux.visible
import com.starbucks.astapley.sbux.weather.WeatherViewState.WeatherError
import com.starbucks.astapley.sbux.weather.WeatherViewState.WeatherLoading
import com.starbucks.astapley.sbux.weather.WeatherViewState.WeatherResult
import kotlinx.android.synthetic.main.fragment_weather.*
import org.koin.android.viewmodel.ext.android.viewModel

class WeatherFragment : BaseFragment() {

    private val viewModel by viewModel<WeatherViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_weather, container, false)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getForecast().observe(this, Observer { updateViewState(it) })
    }

    private fun updateViewState(state: WeatherViewState) = when (state) {
        WeatherLoading -> loadingView.visible()
        is WeatherResult -> showWeatherResult(state.forecast)
        is WeatherError -> showWeatherError(state.message)
    }

    private fun showWeatherResult(forecast: Forecast) {
        weatherTextView.text = forecast.toString()
        loadingView.gone()
    }

    private fun showWeatherError(message: String) {
        weatherTextView.text = message
        loadingView.gone()
    }
}

package com.starbucks.astapley.sbux.helpers

import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.starbucks.astapley.sbux.helpers.FirebaseAuthResult.Failure
import com.starbucks.astapley.sbux.helpers.FirebaseAuthResult.Success
import com.starbucks.astapley.sbux.profile.AuthEvent
import io.reactivex.Observable
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

sealed class FirebaseAuthResult {
    data class Success(val user: FirebaseUser) : FirebaseAuthResult()
    data class Failure(val errorMessage: String) : FirebaseAuthResult()
}

suspend fun FirebaseAuth.signIn(event: AuthEvent): FirebaseAuthResult =
    suspendCoroutine { cont ->
        signInWithEmailAndPassword(event.email, event.password)
            .addOnSuccessListener { result ->
                result.user?.also {
                    cont.resume(Success(it))
                } ?: run {
                    cont.resume(Failure("User was null"))
                }
            }
            .addOnFailureListener { error ->
                error.localizedMessage?.also {
                    cont.resume(Failure(it))
                } ?: run {
                    cont.resume(Failure("Authentication failed for an unknown reason"))
                }
            }
    }

open class FirebaseAuthCoroutine {

    private val auth: FirebaseAuth
        get() = FirebaseAuth.getInstance()

    val currentUser: FirebaseUser?
        get() = auth.currentUser

    fun join(event: AuthEvent): Observable<AuthResult> =
        auth.createUserWithEmailAndPassword(event.email, event.password).toObservable()

    suspend fun signIn(event: AuthEvent): FirebaseAuthResult = auth.signIn(event)

    fun signOut() {
        auth.signOut()
    }
}